cc = gcc

project = ctemp

include = -Isrc/

src = src/*.c
src += src/commands/*.c

op_flags = -Wall -Werror -O2

debug = OFF

all: compile

compilation = $(cc) $(src) -o $(project) $(include) $(op_flags)

ifeq ($(debug),ON)
	$(info "Building binary in debug mode")
	compilation += -g
endif

compile:
	$(compilation)
