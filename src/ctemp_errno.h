#ifndef CTEMP_ERRNO_H_
#define CTEMP_ERRNO_H_

#include <stdint.h>

#define CTEMP_ERRNO_NULL     -0x01
#define CTEMP_ERRNO_DATA     -0x02
#define CTEMP_ERRNO_COPY     -0x03
#define CTEMP_ERRNO_EXISTS   -0x04
#define CTEMP_ERRNO_OVERFLOW -0x05

static int32_t ctemp_errno = 0;

#ifdef CTEMP_DEBUG
#   include <stdio.h>
#   define CTEMP_DEBUG_LOG(STR, ...) printf("[ERROR %d] " STR "\n", ctemp_errno, __VA_ARGS__)
#else
#   define CTEMP_DEBUG_LOG(STR, ...)
#endif

#endif
