#include "ctemp_command.h"

CTemp_Command CTemp_Command_Create(char abrv, const char *name, void (*function)(CTemp_Data*)) {
    CTemp_Command cmd;
    cmd.abrv[0] = abrv;
    cmd.abrv[1] = '\0';
    strcpy(cmd.name, name);
    cmd.function = function;

    add_prefix(CTEMP_ABRV_PREFIX, cmd.abrv);
    add_prefix(CTEMP_CMD_PREFIX, cmd.name);

    return cmd;
}
