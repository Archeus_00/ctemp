#include "ctemp_io.h"

#include "ctemp_errno.h"
#include <stdio.h>
#include <stdlib.h>

void CTemp_IO_FileToStr(const char *path, char **data, uint64_t *size){
    FILE *file = fopen(path, "rb");
    if(!file){
        ctemp_errno = CTEMP_ERRNO_NULL;
        CTEMP_DEBUG_LOG("fopen(%s, \"rb\"", path);
        return;
    }

    fseek(file, 0L, SEEK_END);
    *size = ftell(file);
    if(!*size){
        ctemp_errno = CTEMP_ERRNO_DATA;
        CTEMP_DEBUG_LOG("*size = %lu", *size);
        return;
    }

    rewind(file);
    *data = (char *) calloc(1, *size + 1);
    if(!*data){
        fclose(file);
        ctemp_errno = CTEMP_ERRNO_NULL;
        CTEMP_DEBUG_LOG("calloc(1, %lu + 1", *size);
        return;
    }

    if(1 != fread(*data, *size, 1, file)){
        fclose(file);
        ctemp_errno = CTEMP_ERRNO_COPY;
        CTEMP_DEBUG_LOG("fread(*data, %lu, 1, file)", *size);
        return;
    }

    fclose(file);
}
