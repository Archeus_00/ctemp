#ifndef CTEMP_CONFIG_H_
#define CTEMP_CONFIG_H_

#include <stdint.h>

/**
 * @brief a vector of sections
 */
typedef struct CTemp_Config CTemp_Config;

/**
 * @brief creates CTemp_Config type
 *
 * @param config CTemp_Config to create
 */
void CTemp_Config_Create(CTemp_Config **config);

/**
 * @brief destroyes CTemp_Config type
 *
 * @param config CTemp_Config to destory
 */
void CTemp_Config_Destroy(CTemp_Config *config);

/**
 * @brief adds section to config type
 * 
 * @param config CTemp_Config to add section to
 * @param id     unique id of section that is being added
 * @param data   section data that is being added
 */
void CTemp_Config_AddSection(CTemp_Config *config, uint32_t id, char **data);

#endif // CTEMP_CONFIG_H_
