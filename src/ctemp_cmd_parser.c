#include "ctemp_cmd_parser.h"

int CTemp_Check_Command(CTemp_Data *data, CTemp_Command *cmd){
    int success = 0;
    for (uint8_t i = 0; i < data->argc; i++){
        if(!strcmp(data->argv[i], cmd->name) || !strcmp(data->argv[i], cmd->abrv)){
            success = 1;
        }
    }
    return success;
}

void CTemp_Execute_Command(CTemp_Data *data, CTemp_Command *cmd){
    if(cmd->function){
        (*cmd->function)(data);
    }
    else
        puts("Failed to read input");
}
