#ifndef CTEMP_CMD_PARSER_H_
#define CTEMP_CMD_PARSER_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "ctemp_command.h"
#include "ctemp_data.h"

int CTemp_Check_Command(CTemp_Data* data, CTemp_Command* cmd);

/*
 * @brief Execute the command if cmd->function != NULL and cmd exists
 *
 * @param data is a pointer to the instance of CTemp_Data
 * @param cmd is a pointer to the command that is called
 */
void CTemp_Execute_Command(CTemp_Data* data, CTemp_Command* cmd);

#endif
