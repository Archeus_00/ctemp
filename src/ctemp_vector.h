#ifndef CTEMP_VECTOR_H_
#define CTEMP_VECTOR_H_

#include <stdint.h>

/**
 * @brief a dynamic array type
 */
typedef struct CTemp_Vector CTemp_Vector;

/**
 * @brief creates CTemp_Vector type
 *
 * @param vector CTemp_Vector to create
 */
void CTemp_Vector_Create(CTemp_Vector **vector);

/**
 * @brief destroyes CTemp_Vector type
 *
 * @param vector CTemp_Vector to destory
 */
void CTemp_Vector_Destroy(CTemp_Vector *vector);

/**
 * @brief adds value to vector
 *
 * @param vector CTemp_Vector to add to
 * @param data   data that is being added
 */
void CTemp_Vector_Add(CTemp_Vector *vector, void *data);

/**
 * @brief remove from CTemp_Vector
 *
 * @param vector CTemp_Vector to remove from
 * @param data   data that is being removed
 */
void CTemp_Vector_Remove(CTemp_Vector *vector, void *data);

/**
 * @brief gets size of vector
 *
 * @param vector CTemp_Vector to get size from
 */
uint32_t *CTemp_Vector_Size(CTemp_Vector *vector);

/**
 * @brief gets data from Ctemp_Vector at position index
 *
 * @param vector CTemp_Vector to get data from
 * @param index  position of data in CTemp_Vector
 *
 * @return pointer to data on success, NULL on fail
 */
void *CTemp_Vector_Get(CTemp_Vector *vector, uint32_t *index);

#endif // CTEMP_VECTOR_H_