#include "ctemp_config.h"
#include "ctemp_vector.h"
#include <stdlib.h>

struct CTemp_Config {
    CTemp_Vector* sections;
};

typedef struct CTemp_ConfigSection {
    uint32_t id;
    char **data;
} CTemp_ConfigSection;

void CTemp_Config_Create(CTemp_Config **config){
    *config = (CTemp_Config *) malloc(sizeof(CTemp_Config));
    CTemp_Vector_Create(&((*config)->sections));
}

void CTemp_Config_Destroy(CTemp_Config *config){
    for(uint32_t i = 0; i < *CTemp_Vector_Size(config->sections); i++){
        CTemp_ConfigSection *section = (CTemp_ConfigSection *) CTemp_Vector_Get(config->sections, &i);
        free(section->data);
        free(section);
    }

    CTemp_Vector_Destroy(config->sections);

    free(config);
}

void CTemp_Config_AddSection(CTemp_Config *config, uint32_t id, char **data){
    CTemp_ConfigSection *section = (CTemp_ConfigSection *) malloc(sizeof(CTemp_ConfigSection));
    *section = (CTemp_ConfigSection) { id, data };
    CTemp_Vector_Add(config->sections, (void *)section);
}
