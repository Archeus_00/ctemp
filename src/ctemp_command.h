#ifndef CTEMP_COMMAND_H_
#define CTEMP_COMMAND_H_

#include "ctemp_utils.h"
#include "ctemp_data.h"

#if !defined(CTEMP_PREFIX)
#   define CTEMP_PREFIX
#       define CTEMP_ABRV_PREFIX "-"
#       define CTEMP_CMD_PREFIX  "--"
#endif

typedef struct CTemp_Command{
    char abrv[3];
    char name[0xff];
    void (*function)(CTemp_Data* data);
} CTemp_Command;

/*
 * @brief Create a new command
 *
 * @param abrv is an abbreviation for the command's name
 *
 * @param name is the actual full name of the command
 *
 * @param function is a pointer to the function the command will execute once its called
 */
CTemp_Command CTemp_Command_Create(char abrv, const char* name, void (*function)(CTemp_Data*));

#endif
