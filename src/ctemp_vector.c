#include "ctemp_vector.h"
#include "ctemp_errno.h"
#include <stdlib.h>
#include <string.h>

struct CTemp_Vector {
    uint32_t *size, *capacity;
    void **data;
};

void CTemp_Vector_Create(CTemp_Vector **vector){
    *vector = (CTemp_Vector *) malloc(sizeof(CTemp_Vector));
    (*vector)->size     = (uint32_t  *) malloc(sizeof(uint32_t));
    (*vector)->capacity = (uint32_t  *) malloc(sizeof(uint32_t));
    (*vector)->data     = (void     **) malloc(sizeof(void *));

    *((*vector)->size)     = 0;
    *((*vector)->capacity) = 1;
}

void CTemp_Vector_Destroy(CTemp_Vector *vector){
    free(vector->size);
    free(vector->capacity);
    free(vector->data);
    free(vector);
}

void CTemp_Vector_Add(CTemp_Vector *vector, void *data){
    if(*(vector->size) == ~((uint32_t)0)){
        ctemp_errno = CTEMP_ERRNO_OVERFLOW;
        return;
    }

    if(*(vector->size) == *(vector->capacity)){
       *(vector->capacity) <<= 1;

        vector->data = (void **) realloc(vector->data, sizeof(void *) * (*(vector->capacity)));
    }

    vector->data[*(vector->size)] = data;
    ++*(vector->size);
}

void CTemp_Vector_Remove(CTemp_Vector *vector, void *data){
    if(!*(vector->size)){
        ctemp_errno = CTEMP_ERRNO_DATA;
        return;
    }

    --*(vector->size);

    if(*(vector->size) != *(vector->capacity) >> 1){
        for(uint32_t i = 0; i < *(vector->size); i++){
            if(data == vector->data[i]){
                memcpy(vector->data + i, vector->data + i + 1, *(vector->size) - i);
                vector->data[*(vector->size) + 1] = NULL;
                return;
            }
        }

        ctemp_errno = CTEMP_ERRNO_DATA;
        return;
    }

    *(vector->capacity) >>= 1;
    void **temp = (void **) malloc(sizeof(void *) * (*(vector->capacity)));

    for(uint32_t i = 0; i < *(vector->size); i++){
        if(data == vector->data[i]){
            memcpy(temp + i, vector->data, *(vector->size) - i); free(vector->data);
            vector->data = temp;
            break;
        }
        temp[i] = vector->data[i];
    }

    free(temp);
    ctemp_errno = CTEMP_ERRNO_DATA;
}

uint32_t *CTemp_Vector_Size(CTemp_Vector *vector){ return vector->size; }

void *CTemp_Vector_Get(CTemp_Vector *vector, uint32_t *index){ return vector->data[*index]; }
