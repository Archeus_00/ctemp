// Standard includes
#include <stdio.h>
#include <stdlib.h>

// Project includes
#include "ctemp_io.h"
#include "ctemp_utils.h"
#include "ctemp_command.h"
#include "ctemp_config.h"
#include "ctemp_data.h"
#include "ctemp_cmd_parser.h"

// All commands here
#include "commands/ctemp_command_list.h"

int main(int argc, char **argv) {
    CTemp_Data data = CTemp_Init(argc, argv);

    CTemp_Command commands[] = {
        CTemp_Command_Create('v', "version", CTemp_CMD_Version),
        CTemp_Command_Create('i', "init", NULL)
    };

    uint8_t iters = (sizeof(commands)/sizeof(CTemp_Command));
    for (uint8_t i = 0; i < iters; i++){
        if(CTemp_Check_Command(&data, &commands[i])){
            CTemp_Execute_Command(&data, &commands[i]);
        }
    }

    return 0;
}
