#ifndef CTEMP_UTILS_H_
#define CTEMP_UTILS_H_

#include <string.h>

// Byte sized temporary array (because we won't be needing bigger commands than that)
static inline void add_prefix(const char* prefix, char* string){
    char tmp[0xff];
    // Deprecated region
    // DANGER: strcpy()
    strcpy(tmp, prefix);
    strcat(tmp, string);
    strcpy(string, tmp);
}

#endif // CTEMP_UTILS_H_
