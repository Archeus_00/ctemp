#include "ctemp_command_list.h"
#include "../app_version.h"

/*
 * @brief Print out ctemp application version
 *
 * @param data is a pointer to the instance of CTemp_Data
 */
void CTemp_CMD_Version(CTemp_Data *data){
#ifndef APP_VERSION
#   define APP_VERSION "Unknown Build"
#endif
    puts(APP_VERSION);
}
