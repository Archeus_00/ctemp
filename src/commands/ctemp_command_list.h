#ifndef CTEMP_CMD_LIST_H_
#define CTEMP_CMD_LIST_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "../ctemp_data.h"

/*
 * @brief Print out the version of the application
 *
 * @param data is a pointer to the instance of CTemp_Data
 */
void CTemp_CMD_Version(CTemp_Data* data);

/*
 * @brief Initialize an empty project with the following configs:
 *      > bin
 *      > src
 *          - main.c/cpp (hello world example)
 *      > perf
 *      - CMakeLists.txt (User config)
 *      - Makefile (Use config)
 * @param data is a pointer to the instance of CTemp_Data
 */
void CTemp_CMD_Init(CTemp_Data* data);

#endif
