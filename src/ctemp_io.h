#ifndef CTEMP_IO_H_
#define CTEMP_IO_H_

#include <stdint.h>

/**
 * @brief get string and size from file
 *
 * @param path a string to path of target file
 * @param data pointer to where string will be created
 *             this will need to be freed once done using it
 * @param size size of string
 *
 * @note ctemp_errno will be set on error
*/
void CTemp_IO_FileToStr(const char *path, char **data, uint64_t *size);

#endif
