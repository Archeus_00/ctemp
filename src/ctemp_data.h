#ifndef CTEMP_DATA_H_
#define CTEMP_DATA_H_

typedef struct CTemp_Data {
    int argc;
    char** argv;
} CTemp_Data;

/*
 * @brief Initialize data structure, it includes the core functionalities
 *
 * @param argc is the arguement count
 *
 * @param argv is an array of strings passed in the command line as arguements
 */
CTemp_Data CTemp_Init(int argc, char** argv);

#endif
